package edu.neu.madcourse.mads17_finalproject;

import android.util.Log;

public class PagerSingleton {
    private static PagerSingleton pagerSingleton = new PagerSingleton();
    private static final String TAG = "PagerSingleton";
    private int inventoryPage;
    private int storePage;


    public static PagerSingleton getInstance() {
        return pagerSingleton;
    }

    private PagerSingleton() {
        inventoryPage = 0;
        storePage = 0;
    }

    public int getStorePage() {
        return storePage;
    }

    public void setStorePage(int storePage) {
        this.storePage = storePage;
    }

    public int getInventoryPage() {
        return inventoryPage;
    }

    public void setInventoryPage(int inventoryPage) {
        Log.d(TAG, "Setting inventory page to " + Integer.toString(inventoryPage));
        this.inventoryPage = inventoryPage;
    }
}
