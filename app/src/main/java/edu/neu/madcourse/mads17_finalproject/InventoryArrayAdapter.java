package edu.neu.madcourse.mads17_finalproject;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.ArrayList;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;
import edu.neu.madcourse.mads17_finalproject.models.User;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static edu.neu.madcourse.mads17_finalproject.R.string.coins;

public class InventoryArrayAdapter extends BaseAdapter {
    public Context context;
    public ArrayList<Item> items;
    public String type;
    private static LayoutInflater inflater = null;
    private static final String TAG = "StoreArrAdapter";

    public InventoryArrayAdapter(InventoryActivity inventoryActivity, ArrayList<Item> items, String type){
        this.context = inventoryActivity;
        this.items = items;
        this.type = type;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void setViewPagerPosition(String type){
        Log.d(TAG, "set view pager position");
        Log.d(TAG, type);
        switch(type) {
            case "dino":
                PagerSingleton.getInstance().setInventoryPage(0);
                return;
            case "plant":
                PagerSingleton.getInstance().setInventoryPage(1);
                return;
            case "powerUp":
                PagerSingleton.getInstance().setInventoryPage(2);
                return;
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);

        View rowView;
        if(item.getActive()){
            rowView = getActiveItemView(item, position);
        } else {
            rowView = getInactiveItemView(item, position);
        }
        return rowView;
    }

    public View getActiveItemView(Item item, int position){
        final ViewHolder holder = new ViewHolder();
        final int pos = position;
        View rowView = inflater.inflate(R.layout.inventory_item_active, null);
        holder.itemName = (TextView) rowView.findViewById(R.id.inventory_item_name);
        holder.icon = (ImageView) rowView.findViewById(R.id.inventory_item_icon);
        holder.itemDescription = (TextView) rowView.findViewById(R.id.inventory_item_description);

        holder.item = item;
        holder.itemName.setText(item.getItemName());
        holder.itemDescription.setText(item.getDescription());
        holder.position = position;

        int iconId = getImageId(item);
        holder.icon.setImageResource(iconId);


        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setViewPagerPosition(holder.item.getType());
                if (!holder.item.getType().equals("powerUp")) {
                    toggleItem(items.get(pos));
                    updateItems(items);
                }
            }
        });
        return rowView;
    }

    public View getInactiveItemView(Item item, int position){
        final ViewHolder holder = new ViewHolder();
        final int pos = position;
        View rowView = inflater.inflate(R.layout.inventory_item_inactive, null);
        holder.itemName = (TextView) rowView.findViewById(R.id.inventory_item_name_inactive);
        holder.icon = (ImageView) rowView.findViewById(R.id.inventory_item_icon_inactive);
        holder.itemDescription = (TextView) rowView.findViewById(R.id.inventory_item_description_inactive);

        holder.item = item;
        holder.itemName.setText(item.getItemName());
        holder.itemDescription.setText(item.getDescription());
        holder.position = position;

        int iconId = getImageId(item);

        holder.icon.setImageResource(iconId);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setViewPagerPosition(holder.item.getType());
                    if (!holder.item.getType().equals("powerUp")) {
                        String currentType = items.get(pos).getType();
                        User user = UserSingleton.getInstance().getUser();
                        ItemCollection userItems = user.getItemCollection();
                        Integer numActive = userItems.getActiveSublist(currentType).size();
                        Integer maxActive = IslandActivity.getMaxActiveAllowed(currentType);

                        // If they're trying to make the item active but we've already reached the max number
                        // of active items allowed, don't let them toggle it.
                        if (numActive >= maxActive) {
                            String toastMessage = "You already have the max amount of " + currentType + "s, " + maxActive.toString()
                                    + ", on your island! Take another " + currentType + " out to put this one in.";
                            Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
                        } else {
                            toggleItem(items.get(pos));
                            updateItems(items);
                        }
                    }
                }
            });
        return rowView;
    }

    // Retrieves the image resource ID for a given image. Returns a placeholder image if the resource
    // is not found.
    public int getImageId(Item item){
        int id = context.getResources().getIdentifier(item.getItemName() + "_icon", "drawable", context.getPackageName());
        if(id == 0){
            id = context.getResources().getIdentifier("dino_placeholder", "mipmap", context.getPackageName());
        }
        return id;
    }

    // Static inner class to prevent unnecessary re-rendering on tab change
    static class ViewHolder {
        TextView itemName;
        TextView itemDescription;
        ImageView icon;
        Item item;
        int position;
    }

    // Toggles an item between active and inactive.
    public void toggleItem(Item item){
        Log.d(TAG, "Toggle item");
        Log.d(TAG, item.toString());
        if(item.getActive()){
            Log.d(TAG, "Setting item from active to inactive");
            item.setActive(false);
        } else {
            Log.d(TAG, "Setting item from inactive to active");
            item.setActive(true);
        }
    }

    // Updates the user in Firebase and the UserSingleton when an item is changed
    // between active/inactive.
    public void updateItems(ArrayList<Item> updatedItems){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final User updatedUser = UserSingleton.getInstance().getUser();
        updatedUser.getItemCollection().setSublist(updatedItems, this.type);
        UserSingleton.getInstance().setUser(updatedUser);
        UserSingleton.getInstance().displayUserInfo();
        mDatabase
                .child("Users")
                .child(updatedUser.getUserKey())
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User user = mutableData.getValue(User.class);
                        if (user == null) {
                            return Transaction.success(mutableData);
                        }
                        mutableData.setValue(updatedUser);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }
}
