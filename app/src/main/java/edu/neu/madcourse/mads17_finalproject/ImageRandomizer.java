package edu.neu.madcourse.mads17_finalproject;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.lang.reflect.Field;
import java.util.Random;


public class ImageRandomizer {

    private static ImageRandomizer instance = null;
    HashMap<String, ArrayList<Integer>> drawables = new HashMap<>();
    HashMap<String, ArrayList<Integer>> animations = new HashMap<>();
    private static final String TAG = "ImageRandomizer";

    public static ImageRandomizer getImageRandomizer(){
        if(instance == null){
            instance = new ImageRandomizer();
        }
        return instance;
    }

    public ArrayList<Integer> getAnimationsForItem(String itemName){
        if(!animations.containsKey(itemName)){
            Field[] fields = R.drawable.class.getFields();
            ArrayList<Integer> images = new ArrayList<>();
            for (Field field : fields) {
                if (field.getName().startsWith(itemName + "animation")) {
                    try {
                        images.add(field.getInt(null));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            animations.put(itemName, images);
        }
        Log.d(TAG, animations.toString());
        return animations.get(itemName);
    }

    public ArrayList<Integer> getDrawablesForItem(String itemName){
        if(!drawables.containsKey(itemName)){
            Field[] fields = R.drawable.class.getFields();
            ArrayList<Integer> images = new ArrayList<>();
            for (Field field : fields) {
                if (field.getName().startsWith(itemName + "__")) {
                    try {
                        images.add(field.getInt(null));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            drawables.put(itemName, images);
        }
        Log.d(TAG, drawables.toString());
        return drawables.get(itemName);
    }

    public int getRandomDrawableIDForItem(String itemName){
        ArrayList<Integer> drawables = getDrawablesForItem(itemName);
        Random randomGenerator = new Random();
        Integer randomIndex = randomGenerator.nextInt(drawables.size());
        return drawables.get(randomIndex);
    }

    public int getRandomAnimationIDForItem(String itemName){
        ArrayList<Integer> mAnimations = getAnimationsForItem(itemName);
        Random randomGenerator = new Random();
        Integer randomIndex = randomGenerator.nextInt(mAnimations.size());
        Log.d(TAG, mAnimations.get(randomIndex).toString());
        return mAnimations.get(randomIndex);
    }


}
