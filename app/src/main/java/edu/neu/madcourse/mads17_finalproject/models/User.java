package edu.neu.madcourse.mads17_finalproject.models;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.mads17_finalproject.UserSingleton;

/**
 * Created by nguyenvyl on 3/6/17.
 */

public class User {
    public String username = "";
    public String userKey = "";
    public Integer totalSteps = 0;
    public Integer stepsToday = 0;
    public Integer coins = 0;
    public String clientToken = "";
    public ItemCollection itemCollection;



    public User(){
    }

    public User(String username, String userKey, String clientToken) {
        this.username = username;
        this.userKey = userKey;
        this.clientToken = clientToken;
        this.totalSteps = new Integer(0);
        this.stepsToday = new Integer(0);
        this.coins = new Integer(0);
        this.itemCollection = new ItemCollection();
    }



    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ItemCollection getItemCollection() {
        return itemCollection;
    }

    public void setItemCollection(ItemCollection itemCollection) {
        this.itemCollection = itemCollection;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }

    public Integer getCoins() {
        return coins;
    }

    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    public Integer getStepsToday() {
        return stepsToday;
    }

    public void setStepsToday(Integer stepsToday) {
        this.stepsToday = stepsToday;
    }

    public Integer getTotalSteps() {
        return totalSteps;
    }

    public void setTotalSteps(final Integer totalSteps) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseUsersReference = database.getReference("Users");



        System.out.println(UserSingleton.getInstance().getUserKeyFromPrefs());

        databaseUsersReference
                .child(UserSingleton.getInstance().getUserKeyFromPrefs())
                .child("totalSteps")
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        mutableData.setValue(totalSteps);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
//                        stopLoading();
//                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);

                    }
                });
//        userScoreReference.setValue(totalSteps);

        this.totalSteps = totalSteps;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", userKey='" + userKey + '\'' +
                ", totalSteps=" + totalSteps +
                ", stepsToday=" + stepsToday +
                ", coins=" + coins +
                ", clientToken='" + clientToken + '\'' +
                ", itemCollection=" + itemCollection +
                '}';
    }

    @Exclude
    public Integer getMultiplier() {
        Integer multiplier = 1;
        if(itemCollection != null){
            if(itemCollection.getSublist("powerUp") != null){
                for(Item powerUp : itemCollection.getSublist("powerUp")){
                    multiplier += powerUp.getMultiplier();
                }
            }
        }
        return multiplier;
    }

    public Boolean owns(Item item){
        if(this.itemCollection == null){
            return false;
        }
        return this.itemCollection.owns(item);
    }

}
