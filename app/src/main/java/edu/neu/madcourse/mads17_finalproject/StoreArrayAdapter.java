package edu.neu.madcourse.mads17_finalproject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import java.util.ArrayList;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;
import edu.neu.madcourse.mads17_finalproject.models.User;

import static edu.neu.madcourse.mads17_finalproject.R.string.coins;


/**
 * Created by nguyenvyl on 4/10/17.
 */

public class StoreArrayAdapter extends BaseAdapter {
    public Context context;
    public ArrayList<Item> items;
    private static LayoutInflater inflater = null;
    private static final String TAG = "StoreArrAdapter";
    private User user;

    public StoreArrayAdapter(StoreActivity storeActivity, ArrayList<Item> items){
        this.context = storeActivity;
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        user = UserSingleton.getInstance().getUser();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = items.get(position);
//        User user = UserSingleton.getInstance().getUser();
        Integer coins = user.getCoins();

        View rowView;
        if(item.getPrice() >= coins || (user.owns(item) && item.getType().equals("powerUp"))){
            rowView = getDisabledStoreItemView(item, position);
        } else {
            rowView = getStoreItemView(item, position);
        }
        return rowView;
    }

    public void setViewPagerPosition(String type){
        Log.d(TAG, "set view pager position");
        Log.d(TAG, type);
        switch(type) {
            case "dino":
                PagerSingleton.getInstance().setStorePage(0);
                return;
            case "plant":
                PagerSingleton.getInstance().setStorePage(1);
                return;
            case "powerUp":
                PagerSingleton.getInstance().setStorePage(2);
                return;
        }
    }

    // Renders a single store item row as a buyable item.
    public View getStoreItemView(Item item, int position){
        final ViewHolder holder = new ViewHolder();
        View rowView = inflater.inflate(R.layout.store_item, null);
        holder.itemName = (TextView) rowView.findViewById(R.id.item_name);
        holder.itemPrice = (TextView) rowView.findViewById(R.id.item_price);
        holder.icon = (ImageView) rowView.findViewById(R.id.item_icon);
        holder.itemDescription = (TextView) rowView.findViewById(R.id.item_description);

        holder.item = item;
        holder.itemName.setText(item.getItemName());
        holder.itemPrice.setText(item.getPrice().toString());
        holder.itemDescription.setText(item.getDescription());
        holder.position = position;

        int iconId = getImageId(item);
        holder.icon.setImageResource(iconId);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setViewPagerPosition(holder.item.getType());
                purchaseItem(holder.item);
            }
        });
        return rowView;
    }

    // Renders a single store item row as a disabled item.
    public View getDisabledStoreItemView(Item item, int position){
        final ViewHolder holder = new ViewHolder();
        View rowView = inflater.inflate(R.layout.store_item_disabled, null);
        holder.itemName = (TextView) rowView.findViewById(R.id.disabled_item_name);
        holder.itemPrice = (TextView) rowView.findViewById(R.id.disabled_item_price);
        holder.icon = (ImageView) rowView.findViewById(R.id.disabled_item_icon);
        holder.itemDescription = (TextView) rowView.findViewById(R.id.disabled_item_description);

        holder.item = item;
        holder.itemName.setText(item.getItemName());
        holder.itemPrice.setText(item.getPrice().toString());
        holder.itemDescription.setText(item.getDescription());
        holder.position = position;

        int iconId = getImageId(item);
        holder.icon.setImageResource(iconId);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setViewPagerPosition(holder.item.getType());
                if(holder.item.getType().equals("powerUp")){
                    Toast.makeText(context, "You've already purchased this power up!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Not enough coins to purchase!", Toast.LENGTH_LONG).show();
                }
            }
        });
        return rowView;
    }

    public int getImageId(Item item){
        int id = context.getResources().getIdentifier(item.getItemName() + "_icon", "drawable", context.getPackageName());
        if(id == 0){
            id = context.getResources().getIdentifier("dino_placeholder", "mipmap", context.getPackageName());
        }
        return id;
    }

    // Static inner class to prevent unnecessary re-rendering on tab change
    static class ViewHolder {
        TextView itemName;
        TextView itemPrice;
        TextView itemDescription;
        ImageView icon;
        Item item;
        int position;
    }

    // When an item is purchased, this updates both the UserSingleton's user and
    // updates the user in Firebase.
    public void purchaseItem(Item item){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        final User updatedUser = UserSingleton.getInstance().getUser();
        // Prevent NullPointerException if the user didn't have any items.
        if(updatedUser.getItemCollection() == null){
            updatedUser.setItemCollection(new ItemCollection());
        }
        //Updates the UserSingleton with the new information.
        updatedUser.getItemCollection().addItem(item);
        updatedUser.setCoins(updatedUser.getCoins() - item.getPrice());
        UserSingleton.getInstance().setUser(updatedUser);
        UserSingleton.getInstance().displayUserInfo();
        Toast.makeText(context, "Item purchased!", Toast.LENGTH_LONG).show();
        // Pushes the updated user to Firebase.
        mDatabase
                .child("Users")
                .child(updatedUser.getUserKey())
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        User user = mutableData.getValue(User.class);
                        if (user == null) {
                            return Transaction.success(mutableData);
                        }
                        mutableData.setValue(updatedUser);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }
}
