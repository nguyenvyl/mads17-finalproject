package edu.neu.madcourse.mads17_finalproject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.net.URL;

import edu.neu.madcourse.mads17_finalproject.models.User;

import static android.os.Build.VERSION_CODES.M;

public class UserSingleton {
    private static UserSingleton instance = null;
    private Context context;
    private DatabaseReference mDatabase;
    Integer numUsers = new Integer(0);
    private String userKey;
    private String username;
    private User user = null;
    private static final String USER_KEY = "user_key";
    private static final String USERNAME = "username";
    private static final String TAG = "UserSingleton";


    private UserSingleton(){
        this.context = MyApplication.getAppContext();
    }

    public static UserSingleton getInstance(){
        if(instance == null){
            instance = new UserSingleton();
        }
        return instance;
    }

    // Loads the user's profile from Firebase.
    public void loadUser(){
        startLoading();
        Log.d(TAG, "loadUser");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "dataSnapshot");
                        if(!userExists()){
                            Log.d(TAG, "user doesn't exist, creating new user...");
                            createNewUser();
                        }
                        else{
                            Log.d(TAG, "user exists, retrieving user...");
                            setSavedUser();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, "on Cancelled Error: " + databaseError.toString());
                    }
                }
        );

        mDatabase.child("Users").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if(userExists()){
                            User firebaseUser = dataSnapshot.getValue(User.class);
                            if(firebaseUser.userKey.equals(getUserKeyFromPrefs())){
                                setUser(firebaseUser);
//                                stopLoading();
                            }
                        }
                        numUsers++;
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled:" + databaseError);
                    }
                }
        );
    }

    private boolean userExists(){
        return getUserKeyFromPrefs() != null;
    }

    public String getUserKeyFromPrefs(){
        return context.getSharedPreferences(context.getPackageName() + "_preferences", 0).getString(USER_KEY, null);
    }

    private String getUsernameFromPrefs(){
        return context.getSharedPreferences(context.getPackageName() + "_preferences", 0).getString(USERNAME, null);
    }


    // Saves the current user's key and username to this singleton to be accessible app-wide.
    public void setSavedUser(){
        Log.d(TAG, "set saved user");
        if(user == null){
            createNewUser();
        } else {
            String userKey = user.getUserKey();
            String username = user.getUsername();
            setUserKey(userKey);
            setUsername(username);
        }
    }

    // Creates a new user if this is the person's first time using the app.
    // Also pushes the new user to Firebase.
    public void createNewUser(){
        Log.d(TAG, "create new user");
        Integer userKey = numUsers + 1;
        String username = "user" + userKey.toString();
        Log.d(TAG, "creating user key " + userKey + " with username " + username);

        context.getSharedPreferences(context.getPackageName() + "_preferences", 0).edit()
                .putString(USER_KEY, userKey.toString())
                .putString(USERNAME, username)
                .apply();

        setUserKey(userKey.toString());
        setUsername(username);
        pushUserToDatabase(mDatabase, userKey.toString(), username);
    }

    // Pushes the user's new account to Firebase.
    public void pushUserToDatabase(DatabaseReference postRef, String userKey, String username){
        String token = FirebaseInstanceId.getInstance().getToken();
        final User newUser = new User(username, userKey, token);
        setUser(newUser);
        Log.d(TAG, "Adding user " + newUser.toString());
        postRef
                .child("Users")
                .child(userKey)
                .runTransaction(new Transaction.Handler() {
                    @Override
                    public Transaction.Result doTransaction(MutableData mutableData) {
                        mutableData.setValue(newUser);
                        return Transaction.success(mutableData);
                    }

                    @Override
                    public void onComplete(DatabaseError databaseError, boolean b,
                                           DataSnapshot dataSnapshot) {
                        // Transaction completed
                        stopLoading();
                        Log.d(TAG, "postTransaction:onComplete:" + databaseError);
                    }
                });
    }


    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    // If the user's already been loaded from Firebase, retrieves the user's information from the
    // current UserSingleton.
    // Otherwise, loads the user's profile from Firebase.
    public User getOrLoadUser() {
        Log.d(TAG, "Get or load user");
        if(this.user == null){
            Log.d(TAG, "user is currently null");
            loadUser();
            return user;
        } else {
            displayUserInfo();
            return user;
        }
    }

    public User getUser() {
        Log.d(TAG, "getUser");
//        Log.d(TAG, user.toString());
        return user;
    }

    public void setUser(User user) {
        Log.d(TAG, "Set User");
//        Log.d(TAG, user.toString());
        this.user = user;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    // Displays a "loading" screen as the user's profile is retreived from Firebase.
    // Without this, we'll run into errors when the user tries to do stuff before their profile
    // is retrieved.
    public void startLoading(){
        Log.d(TAG, "start loading");
        View loadView  = ((Activity)context).findViewById(R.id.thinking);
        loadView.setVisibility(View.VISIBLE);
        LoadingUserTask timer = new LoadingUserTask();
        timer.execute();
    }

    // Removes the "loading" screen.
    public void stopLoading(){
        Log.d(TAG, "stop loading");
        View loadView  = ((Activity)context).findViewById(R.id.thinking);
        loadView.setVisibility(View.GONE);
//        displayUserInfo();
    }

    public void showTimeout(){
        Log.d(TAG, "showTimeout");
        View timeoutView = ((Activity)context).findViewById(R.id.timeout);
        timeoutView.setVisibility(View.VISIBLE);
    }

    public void hideTimeout(){
        Log.d(TAG, "hideTimeout");
        View timeoutView = ((Activity)context).findViewById(R.id.timeout);
        timeoutView.setVisibility(View.GONE);
    }

    // Displays the user's information on the UI after loading it from Firebase.
    public void displayUserInfo(){
        Log.d(TAG, "display user info");
        if(context instanceof MainActivity){
            TextView coinsView = (TextView) ((Activity) context).findViewById(R.id.coins_count);
            coinsView.setText(user.getCoins().toString());
            TextView multiplierView = (TextView) ((Activity) context).findViewById(R.id.multiplier_count);
            multiplierView.setText(user.getMultiplier().toString());
        } else if(context instanceof InventoryActivity || context instanceof StoreActivity) {
            TextView coinsView = (TextView) ((Activity) context).findViewById(R.id.coins_display);
            coinsView.setText(context.getString(R.string.your_coins) + user.getCoins().toString());
            TextView multiplierView = (TextView) ((Activity) context).findViewById(R.id.multiplier_display);
            multiplierView.setText(context.getString(R.string.coins_per_step) + user.getMultiplier().toString());
        }
    }


    private class LoadingUserTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.currentThread();
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(UserSingleton.getInstance().user == null){
                showTimeout();
                Log.d(TAG, "User is still null");
            } else {
                displayUserInfo();
            }
            UserSingleton.getInstance().stopLoading();
        }
    }

}
