package edu.neu.madcourse.mads17_finalproject;

import android.app.Application;
import android.content.Context;

/**
 * Created by nguyenvyl on 2/28/17.
 */

public class MyApplication extends Application {

    private static Context mContext;
    private static Application mApplication;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mApplication = this;
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static Application getApp(){
        return mApplication;
    }



}
