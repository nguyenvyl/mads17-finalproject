package edu.neu.madcourse.mads17_finalproject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;


public class ControlFragment extends Fragment {
    public ControlFragment() {
        // Required empty public constructor
    }


    public static ControlFragment newInstance() {
        ControlFragment fragment = new ControlFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_control, container, false);

        View stepsButton = rootView.findViewById(R.id.steps_button);
        View islandButton = rootView.findViewById(R.id.island_button);
        View storeButton = rootView.findViewById(R.id.store_button);
        View inventoryButton = rootView.findViewById(R.id.inventory_button);

        stepsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.mads17_finalproject.MainActivity.class);
                getActivity().startActivity(intent);
            }
        });

        islandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.mads17_finalproject.IslandActivity.class);
                getActivity().startActivity(intent);
            }
        });

        storeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.mads17_finalproject.StoreActivity.class);
                getActivity().startActivity(intent);
            }
        });

        inventoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), edu.neu.madcourse.mads17_finalproject.InventoryActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }
}
