package edu.neu.madcourse.mads17_finalproject;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;
import edu.neu.madcourse.mads17_finalproject.models.User;

import static android.util.Log.d;

public class InventoryActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    private static final String TAG = "InventoryActivity";
    public ItemCollection userItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        d(TAG, "Inventory Activity On Create");
        setContentView(R.layout.activity_inventory);

        // Sets the UserSingleton's context to the current activity.
        UserSingleton.getInstance().setContext(this);
        UserSingleton.getInstance().displayUserInfo();

        String userKey = UserSingleton.getInstance().getUserKey();

        // Retrieves the current user's list of items
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase
                .child("Users")
                .child(userKey)
                .addValueEventListener(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                UserSingleton.getInstance().setUser(user);
                                if(user.getItemCollection() == null){
                                    userItems = new ItemCollection();
                                } else {
                                    userItems = user.getItemCollection();
                                }

                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        }
                );
        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ViewPager viewPager = (ViewPager) findViewById(R.id.inventory_viewpager);
                        InventoryFragmentPagerAdapter pagerAdapter = new InventoryFragmentPagerAdapter(getSupportFragmentManager(), InventoryActivity.this);
                        pagerAdapter.setUserItems(userItems);
                        viewPager.setAdapter(pagerAdapter);
                        viewPager.setCurrentItem(PagerSingleton.getInstance().getInventoryPage());
                        TabLayout tabLayout = (TabLayout) findViewById(R.id.inventory_sliding_tabs);
                        tabLayout.setupWithViewPager(viewPager);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    @Override
    public void onStart(){
        d(TAG, "Inventory Activity On Start");
        super.onStart();
    }

    @Override
    public void onPause(){
        d(TAG, "Inventory Activity On Pause");
        super.onPause();
    }


    @Override
    public void onResume(){
        d(TAG, "Inventory Activity On Resume");
        super.onResume();
    }

    @Override
    public void onStop(){
        d(TAG, "Inventory Activity On Stop");
        super.onStop();
    }

    @Override
    public void onDestroy(){
        d(TAG, "Inventory Activity On Destroy");
        super.onDestroy();
    }

}
