package edu.neu.madcourse.mads17_finalproject.models;

/**
 * Created by nguyenvyl on 4/9/17.
 */

public class Item {
    String type;
    Integer price;
    Boolean active;
    Integer position;
    String itemName;
    Integer multiplier;
    String description;

    public Item() {
    }

    public Item(Integer price, Boolean active, Integer position, String itemName, String type, Integer multiplier, String description) {
        this.price = price;
        this.active = active;
        this.position = position;
        this.itemName = itemName;
        this.type = type;
        this.multiplier = multiplier;
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Item{" +
                "type='" + type + '\'' +
                ", price=" + price +
                ", active=" + active +
                ", position=" + position +
                ", itemName='" + itemName + '\'' +
                ", multiplier=" + multiplier +
                ", description='" + description + '\'' +
                '}';
    }
}
