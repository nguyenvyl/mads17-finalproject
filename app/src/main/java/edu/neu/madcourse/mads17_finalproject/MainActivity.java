package edu.neu.madcourse.mads17_finalproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.mads17_finalproject.models.User;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static java.lang.reflect.Array.getInt;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final String TAG = "MainActivity";
    public User user;
    private SensorManager sensorManager;
    public TextView stepsView;
    public TextView coinsView;
    public TextView multiplierView;
    boolean activityRunning;

    public Integer initialCountValue = 0;
    public FirebaseDatabase database = null;

    public FirebaseDatabase getDatabase() {
        return database;
    }

    public void setDatabase(FirebaseDatabase database) {
        this.database = database;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Main activity on create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UserSingleton.getInstance().setContext(this);
        this.user = UserSingleton.getInstance().getOrLoadUser();

        coinsView = (TextView) findViewById(R.id.coins_count);
        stepsView = (TextView) findViewById(R.id.steps_count);
        multiplierView = (TextView) findViewById(R.id.multiplier_count);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        View cashButton = findViewById(R.id.cash_button);

        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseUsersReference = database.getReference("Users");
                databaseUsersReference
                        .child(UserSingleton.getInstance().getUserKeyFromPrefs())
                        .runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                User mUser = mutableData.getValue(User.class);
                                Integer oldSteps = mUser.getTotalSteps();
                                Integer oldCoins = mUser.getCoins();
                                Integer multiplier = mUser.getMultiplier();

                                Integer newSteps = oldSteps + getUncashedSteps();
                                Integer newCoins = oldCoins + (getUncashedSteps() * multiplier);

                                mUser.setCoins(newCoins);
                                mUser.setTotalSteps(newSteps);

                                mutableData.setValue(mUser);
                                UserSingleton.getInstance().setUser(mUser);
                                return Transaction.success(mutableData);
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                setUncashedSteps(0);
                                stepsView.setText(Integer.toString(getUncashedSteps()));
                                setCoinsViewFromUser();
                            }
                        });
                initialCountValue = 0;
            }
        });

        setDatabase(FirebaseDatabase.getInstance());

        Button retry = (Button) findViewById(R.id.retry_button);
        retry.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserSingleton.getInstance().hideTimeout();
                        UserSingleton.getInstance().startLoading();
                    }
                }
        );
    }


    @Override
    protected void onResume() {
        super.onResume();
        activityRunning = true;
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this, "Count steps sensor not available!", Toast.LENGTH_LONG).show();
        }

        Log.d(TAG, "on resume");
        stepsView.setText(Integer.toString(getUncashedSteps()));

    }

    @Override
    protected void onPause() {
        super.onPause();
        activityRunning = true;
        Double d = Double.parseDouble(stepsView.getText().toString());
        int i = d.intValue();
        setUncashedSteps(i);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        int newSteps;
        if (activityRunning) {
            if(initialCountValue == 0){
                initialCountValue=Math.round(event.values[0]);
                newSteps = Math.round(event.values[0] - initialCountValue) + getUncashedSteps();
            } else {
                newSteps = Math.round(event.values[0] - initialCountValue);
            }
            setUncashedSteps(newSteps);
            stepsView.setText(Integer.toString(getUncashedSteps()));
            setCoinsViewFromUser();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public Integer getUncashedSteps() {
        return getIntFromSharedPrefs("uncashedSteps");
    }

    public void setUncashedSteps(Integer uncashedSteps) {
        addIntToSharedPrefs("uncashedSteps", uncashedSteps);
    }

    public void addIntToSharedPrefs(String key, int i) {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName() + "_preferences", 0);
        sharedPreferences.edit()
                .putInt(key, i)
                .apply();
    }

    public int getIntFromSharedPrefs(String key) {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName() + "_preferences", 0);
        return sharedPreferences.getInt(key, 0);
    }

    public void setCoinsViewFromUser() {
        if(UserSingleton.getInstance().getUser() != null){
            coinsView.setText(UserSingleton.getInstance().getUser().getCoins().toString());
        }
    }

}

