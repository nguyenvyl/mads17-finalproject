package edu.neu.madcourse.mads17_finalproject;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;

/**
 * Created by nguyenvyl on 4/5/17.
 */

public class StoreFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Dinos", "Plants", "Power-Ups"};
    private StoreActivity context;
    private ItemCollection storeItems;
    private static final String TAG = "StoreFragPageAdpt";


    public StoreFragmentPagerAdapter(FragmentManager fm, StoreActivity context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        String type = "";
        switch(position){
            case 0:
                type = "dino";
                break;
            case 1:
                type = "plant";
                break;
            case 2:
                type = "powerUp";
                break;
        }

        return StoreFragment.newInstance(context, type, storeItems.getSublist(type));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException npe){
            Log.d(TAG, "Ignore the NPE in StoreFragmentPagerAdapter.finishUpdate");
        }
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.custom_tab_textview);
        return v;
    }

    public ItemCollection getStoreItems() {
        return storeItems;
    }

    public void setStoreItems(ItemCollection storeItems) {
        Log.d(TAG, "set store items");
        Log.d(TAG, storeItems.toString());
        this.storeItems = storeItems;
    }


}
