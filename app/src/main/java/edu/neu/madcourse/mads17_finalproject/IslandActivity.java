package edu.neu.madcourse.mads17_finalproject;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;
import edu.neu.madcourse.mads17_finalproject.models.User;

import static android.R.attr.animation;
import static edu.neu.madcourse.mads17_finalproject.ImageRandomizer.getImageRandomizer;

public class IslandActivity extends AppCompatActivity {
    static public int[] dinoIds =
            {R.id.dino_slot_1, R.id.dino_slot_2, R.id.dino_slot_3, R.id.dino_slot_4, R.id.dino_slot_5,
                    R.id.dino_slot_6, R.id.dino_slot_7, R.id.dino_slot_8, R.id.dino_slot_9, R.id.dino_slot_10};
    static public int[] plantIds =
            {R.id.plant_slot_1, R.id.plant_slot_2, R.id.plant_slot_3, R.id.plant_slot_4, R.id.plant_slot_5,
                    R.id.plant_slot_6, R.id.plant_slot_7, R.id.plant_slot_8, R.id.plant_slot_9, R.id.plant_slot_10};

    static private String PLANT = "plant";
    static private String DINO = "dino";
    ArrayList<Item> activeDinos;
    ArrayList<Item> activePlants;
    private static final String TAG = "IslandActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_island);

        Log.d(TAG, "oncreate");
        User currentUser = UserSingleton.getInstance().getUser();
        Log.d(TAG, currentUser.toString());

        ItemCollection items = currentUser.getItemCollection();
        if(items != null){
            this.activeDinos = items.getActiveSublist(DINO);
            this.activePlants = items.getActiveSublist(PLANT);
        }

        final FrameLayout islandView = (FrameLayout) findViewById(R.id.island_frame);
        final ArrayList<Item> dinosToRender = this.activeDinos;
        final ViewTreeObserver observer = islandView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(dinosToRender != null){
                    Collections.shuffle(dinosToRender);
                    Log.d(TAG, "Island width: " + Integer.toString(islandView.getHeight()));
                    Log.d(TAG, "Island height: " + Integer.toString(islandView.getHeight()));
                    randomizePositions(dinoIds, islandView.getWidth(), islandView.getHeight());
                    renderItems(dinoIds, dinosToRender);
                }
                islandView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        renderItems(plantIds, this.activePlants);
    }

    public void randomizePositions(int[] viewIds, int width, int height){
        Log.d(TAG, "randomize positions");

        for(int i = 0; i < viewIds.length; i++){
            View view = findViewById(viewIds[i]);
            Log.d(TAG, view.toString());
            int randomWidth = getRandomNum(width, 0);
            int randomHeight = getRandomNum(height, 0);
            Log.d(TAG, "Width: " +  Integer.toString(randomWidth));
            Log.d(TAG, "Height: " + Integer.toString(randomHeight));
            view.setX(randomHeight);
            view.setY(randomWidth);
        }
    }

    public int getRandomNum(int max, int min){
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public void renderItems(int[] ids, ArrayList<Item> items){
        Log.d(TAG, "render items");
//        Log.d(TAG, Integer.toString(items.size()));
        if(items != null && items.size() > 0){
            Log.d(TAG, "Rendering " + items.get(0).getType());
            int num = Math.min(ids.length, items.size());
            for(int i = 0; i < num; i++){
                Item currentItem = items.get(i);
                int imgId = getImageID(currentItem);
                ImageView imgSlot = (ImageView) findViewById(ids[i]);

                if(currentItem.getType().equals("dino")){
                    imgSlot.setBackgroundResource(imgId);
                    AnimationDrawable imgAnimation = (AnimationDrawable) imgSlot.getBackground();
                    if(imgAnimation != null){
                        imgAnimation.start();
                    }
                } else {
                    imgSlot.setImageResource(imgId);
                }
            }
        }
    }

    public static Integer getMaxActiveAllowed(String type){
        switch(type){
            case "dino":
                return dinoIds.length;
            case "plant":
                return plantIds.length;
            default:
                return 0;
        }
    }

    public Integer getImageID(Item item){
        if(item.getType().equals("dino")){
            return ImageRandomizer.getImageRandomizer().getRandomAnimationIDForItem(item.getItemName());
        }
        return getResources().getIdentifier(item.getItemName(), "drawable", getPackageName());
    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        if(hasFocus){
//            ImageView imgSlot = (ImageView) findViewById(R.id.animation_test);
//            imgSlot.setBackgroundResource(R.drawable.animation_test);
//            AnimationDrawable imgAnimation = (AnimationDrawable) imgSlot.getBackground();
//
//            if(imgAnimation != null){
//                imgAnimation.start();
//            }
//
//        }
//    }

}
