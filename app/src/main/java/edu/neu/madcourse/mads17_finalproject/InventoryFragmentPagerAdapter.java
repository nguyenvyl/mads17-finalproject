package edu.neu.madcourse.mads17_finalproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;

/**
 * Created by nguyenvyl on 4/5/17.
 */

public class InventoryFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Dinos", "Plants", "Power-Ups"};
    private InventoryActivity context;
    private ItemCollection userItems;
    private static final String TAG = "InventoryFragPageAdpt";

    public InventoryFragmentPagerAdapter(FragmentManager fm, InventoryActivity context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        String type = "";
        switch(position){
            case 0:
                type = "dino";
                break;
            case 1:
                type = "plant";
                break;
            case 2:
                type = "powerUp";
                break;
        }
        return InventoryFragment.newInstance(context, type, userItems.getSublist(type));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException npe){
            Log.d(TAG, "Ignore the NPE in InventoryFragmentPagerAdapter.finishUpdate");
        }
    }

    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        return v;
    }

    public ItemCollection getUserItems() {
        return userItems;
    }

    public void setUserItems(ItemCollection userItems) {
        this.userItems = userItems;
    }

}
