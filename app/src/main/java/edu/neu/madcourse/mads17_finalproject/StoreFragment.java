package edu.neu.madcourse.mads17_finalproject;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StoreFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private static final String TAG = "StoreFragment";
    public ArrayList<Item> items = new ArrayList<>();
    public String type;
    StoreActivity context;

    public StoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ItemsFragment.
     */
    public static StoreFragment newInstance(StoreActivity context, String type, ArrayList<Item> items) {
        Log.d(TAG, "store fragment new instance");
        StoreFragment fragment = new StoreFragment();
        fragment.setType(type);
        fragment.setItems(items);
        fragment.setContext(context);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "store fragment on create");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "store fragment on create view");
        View view = inflater.inflate(R.layout.fragment_store, container, false);
        final ListView listView = (ListView) view.findViewById(R.id.store_listview);
        listView.setAdapter(new StoreArrayAdapter(this.context, items));

//        TextView textView = (TextView) view.findViewById(R.id.store_fragment_textview);
//        textView.setText("Fragment type:" + type);
//        TextView itemsView = (TextView) view.findViewById(R.id.store_fragment_items);
//        itemsView.setText(items.toString());
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public StoreActivity getContext() {
        return context;
    }

    public void setContext(StoreActivity context) {
        this.context = context;
    }

    @Override
    public String toString() {
        return "StoreFragment{" +
                "mListener=" + mListener +
                ", items=" + items +
                ", type='" + type + '\'' +
                ", context=" + context +
                '}';
    }
}
