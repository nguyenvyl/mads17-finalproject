package edu.neu.madcourse.mads17_finalproject.models;

import android.util.Log;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;

import static android.R.attr.type;

/**
 * Created by nguyenvyl on 4/9/17.
 */

public class ItemCollection {
    ArrayList<Item> dinos;
    ArrayList<Item> plants;
    ArrayList<Item> powerUps;
    private static final String TAG = "ItemCollection";

    public ItemCollection() {
        this.dinos = new ArrayList<>();
        this.plants = new ArrayList<>();
        this.powerUps = new ArrayList<>();
    }


    public void addItem(Item item){
        switch(item.getType()){
            case "dino":
                this.dinos.add(item);
                return;
            case "plant":
                this.plants.add(item);
                return;
            case "powerUp":
                this.powerUps.add(item);
                return;
            default:
                return;
        }
    }

    @Override
    public String toString() {
        return "ItemCollection{" +
                "dinos=" + dinos +
                ", plants=" + plants +
                ", powerUps=" + powerUps +
                '}';
    }

    public ArrayList<Item> getSublist(String type){
        switch(type){
            case "dino":
                return this.dinos;
            case "plant":
                return this.plants;
            case "powerUp":
                return this.powerUps;
            default:
                return this.dinos;
        }
    }

    public ArrayList<Item> getDinos() {
        return dinos;
    }

    public void setDinos(ArrayList<Item> dinos) {
        this.dinos = dinos;
    }

    public ArrayList<Item> getPlants() {
        return plants;
    }

    public void setPlants(ArrayList<Item> plants) {
        this.plants = plants;
    }

    public ArrayList<Item> getPowerUps() {
        return powerUps;
    }

    public void setPowerUps(ArrayList<Item> powerUps) {
        this.powerUps = powerUps;
    }

    public static String getTAG() {
        return TAG;
    }

    public void setSublist(ArrayList<Item> sublist, String type){
        Log.d(TAG, "setSublist");
        Log.d(TAG, type);
        Log.d(TAG, sublist.toString());
        switch(type){
            case "dino":
                this.dinos = sublist;
                return;
            case "plant":
                this.plants = sublist;
                return;
            case "powerUp":
                this.powerUps = sublist;
                return;
            default:
                return;
        }
    }

    @Exclude
    public ArrayList<Item> getActiveSublist(String type){
        ArrayList<Item> activeItems = new ArrayList<>();
        Log.d(TAG, "get active sublist");
        switch (type) {
            case "dino":
                for (Item dino : this.dinos) {
                    if (dino.getActive()){
                        activeItems.add(dino);
                    }
                }
                return activeItems;
            case "plant":
                for (Item plant : this.plants) {
                    if (plant.getActive()){
                        activeItems.add(plant);
                    }
                }
                return activeItems;
            default:
                return activeItems;
        }
    }

    public Boolean owns(Item item){
        switch(item.getType()){
            case "powerUp":
                return checkSublistForItemName(this.powerUps, item.getItemName());
            case "dino":
                return checkSublistForItemName(this.dinos, item.getItemName());
            case "plant":
                return checkSublistForItemName(this.plants, item.getItemName());
            default:
                return false;
        }
    }

    public Boolean checkSublistForItemName(ArrayList<Item> list, String itemName){
        for(Item item : list){
            if(item.getItemName().equals(itemName)){
                return true;
            }
        }
        return false;
    }

}
