package edu.neu.madcourse.mads17_finalproject;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.mads17_finalproject.models.Item;
import edu.neu.madcourse.mads17_finalproject.models.ItemCollection;


public class StoreActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    private static final String TAG = "StoreActivity";
    public ItemCollection storeItems = new ItemCollection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        final ItemCollection items = new ItemCollection();

        UserSingleton.getInstance().setContext(this);
        UserSingleton.getInstance().getUser();
        UserSingleton.getInstance().displayUserInfo();

        mDatabase = FirebaseDatabase.getInstance().getReference();
        Log.d(TAG, mDatabase.toString());
        mDatabase
                .child("Store")
                .addChildEventListener(
                        new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                Log.d(TAG, dataSnapshot.getChildren().toString());
                                Item item = dataSnapshot.getValue(Item.class);
                                items.addItem(item);
                                Log.d(TAG, storeItems.toString());
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {
                                Log.e(TAG, "onChildRemoved: dataSnapshot = " + dataSnapshot.getValue());

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                Log.e(TAG, "onChildMoved: dataSnapshot = " + dataSnapshot.getValue());

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, "onCancelled:" + databaseError);
                            }
                        }
                );

        mDatabase.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
                        StoreFragmentPagerAdapter pagerAdapter = new StoreFragmentPagerAdapter(getSupportFragmentManager(), StoreActivity.this);
                        pagerAdapter.setStoreItems(items);
                        viewPager.setAdapter(pagerAdapter);
                        viewPager.setCurrentItem(PagerSingleton.getInstance().getStorePage());
                        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
                        tabLayout.setupWithViewPager(viewPager);
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                }
        );
    }


}
